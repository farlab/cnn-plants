# Cnn Plants

Code adapted from Sentdex (https://pythonprogramming.net/introduction-deep-learning-python-tensorflow-keras)

Data adapted from UCI MLR One-hundred plant species leaves (https://archive.ics.uci.edu/ml/datasets/One-hundred+plant+species+leaves+data+set)

## Prepare:
Download Zip File: https://archive.ics.uci.edu/ml/machine-learning-databases/00241/100%20leaves%20plant%20species.zip

Unpack Zip File
```
python3 unpack_folders.py
```


## Run:
```
python3 load.py
python3 train.py
python3 predict.py
```
