import cv2
import tensorflow as tf
import os

CATEGORIES = ["quercus", "not_quercus"]
MODELNAME = "64x3-CNN-3-conv-128-nodes-0-dense-1535989287.model"
TEST_DIR = "test"

def prepare(filepath):
    IMG_SIZE = 70
    img_array = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
    new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    return new_array.reshape(-1, IMG_SIZE, IMG_SIZE, 1)


model = tf.keras.models.load_model("models/" + MODELNAME)

print()
for filename in os.listdir(TEST_DIR):
    if filename != ".DS_Store":
        prediction = model.predict([prepare(os.path.join(TEST_DIR, filename))])

        print("%-20s: %s" % (filename, CATEGORIES[int(prediction[0][0])]))