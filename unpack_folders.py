import os
from shutil import copyfile

rootdir = 'Quercus'
outdir = 'quercusx'

for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        copyfile(os.path.join(subdir, file), os.path.join(outdir, file.lower()))
        print(os.path.join(subdir, file), os.path.join(outdir, file.lower()))
